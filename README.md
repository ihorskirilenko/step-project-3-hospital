# Step project #3 - Hospital assistant

# About our star team

<b>Our team name - "Experience"!</b> <br><br>
Oleksii Slavikovskyi - front-end senior SE <br>
```
Logic of visit construction & rendering
Visit class
Dentist class
Visit change
```

Igor Kyrylenko - front-end senior SE
```
Logic of user authorization
User class
Therapist class
Cardiologist class
```

Artem Shevchuk - front-end senior SE
```
All layout
Modal windows, getting user data
Most of listeners
Visits filter
```


# Links to project on our sites

http://kyrylen.co/ <br>
http://slavikovskyi.pro/

## Download, install & run project

https://gitlab.com/ihorskirilenko/step-project-3-hospital

```
git clone https://gitlab.com/ihorskirilenko/step-project-3-hospital.git
npm i
gulp build
gulp dev
```

## Available gulp tasks

gulp clear: removes /dist folder content <br>
gulp css: build css from scss <br>
gulp js: minifying and concat js <br>
gulp img: minifying images <br>
<br>
gulp build: executes gulp clear, css, js, img tasks <br>
gulp dev: calls browser-sync, online rebuilds css, js

# Technologies used:

## Core technologies

HTML<br>
SCSS<br>
Vanilla JS<br>
github-buttons npm plugin<br>
bootstrap<br>

## Dev technologies

gulp<br>
gulp-sass<br>
gulp-concat<br>
gulp-autoprefixer<br>
gulp-clean-css<br>
gulp-clean<br>
gulp-purgecss<br>
gulp-uglify<br>
browser-sync<br>
gulp-imagemin<br>

