import { validate } from './validate.js';
import { modalVisit } from './modalVisit.js';
import { Dentist } from './visitDentist.js';
import { User } from './user.js';
import { deleteAllData } from './deleteAllData.js';
import { getFormData, resetVisitModal } from './getFormData.js';
//export { getFormData } from './getFormData.js';
import { filterCards } from './cardFilter.js';
import { Cardiologist } from './visitCardiologist.js';
import { Therapist } from './visitTherapist.js';
export const token = '86e5fa1b-6548-4dd0-aa30-c269c64a7b35';
export const api = 'https://ajax.test-danit.com/api/v2/cards';
import { Visit } from './visit.js';


filterCards()
modalVisit()

let registration = document.forms.registration;
let signIn = document.forms.signIn;
let visit = document.forms.visitForm

registration.addEventListener('submit', (ev) => {
    ev.preventDefault()
    if (validate()) {
        new User(getFormData()).registration()
        registration.reset()
    }
})

signIn.addEventListener('submit', (ev) => {
    ev.preventDefault()
    new User(getFormData()).signIn()
    signIn.reset()
})


let doctorName = document.querySelector('.doctor-name')

document.querySelector('#visit').addEventListener('click', () => {
    if (doctorName.innerText === 'Кардіолог') {
        new Cardiologist(getFormData()).postVisit();
        visit.reset()
    } else if (doctorName.innerText === 'Дантист') {
        new Dentist(getFormData()).postVisit();
        visit.reset()
    } else if (doctorName.innerText === 'Терапевт') {
        new Therapist(getFormData()).postVisit();
        visit.reset()
    }

})


document.querySelector('.btn-create-visit').addEventListener('click', () => {
    let btnCreateVisit = document.querySelector('#visit');
    btnCreateVisit.classList.remove('none');
    let btnRedactVisit = document.querySelector('.redact');
    btnRedactVisit.classList.add('none');
    const container = document.querySelector('.more-information');
    container.innerHTML = '';
    let prioritiName = document.querySelector('.prioriti-name');
    prioritiName.innerText = 'Виберіть пріорітетність';
    let doctorFild = document.querySelector('.doctor-name');
    doctorFild.innerText = "Виберіть лікаря"
})

//reset inputs
resetVisitModal(document.querySelector('#visitModal form'), document.querySelector('#visitModal .btn-close'));
resetVisitModal(document.querySelector('#regModal form'), document.querySelector('#regModal .btn-close'));
resetVisitModal(document.querySelector('#modalSign form'), document.querySelector('#modalSign .btn-close'));



async function getCards() {

    let request = await fetch(api, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        },

    })
    let data = await request.json()
    console.log(data);

}

getCards()


export function switchButtons() {

    document.querySelectorAll('.toggleHide').forEach(el => el.classList.toggle('hide'))

}

document.querySelector('.btn-danger').addEventListener('click', () => {

    localStorage.removeItem('userId')
    switchButtons()
    document.querySelector('#noElements').classList.add('hide')
    document.querySelectorAll('.wrapper-visit').forEach(el => el.innerHTML = '')

})

window.addEventListener('load', async function () {

    let userId = window.localStorage.getItem('userId')

    if (userId) {
        let user = await fetch(`${api}/${userId}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        new User(user).renderDeskTop()

        switchButtons()
        console.log('im signed!')

    } else {
        console.log('im not signed!')
    }
})































let btnRedactVisit = document.querySelector('.redact');

btnRedactVisit.addEventListener('click', async (event) => {
    let allBtnChangeVisit = document.querySelectorAll('.btn-change-card');
    allBtnChangeVisit.forEach(el => {
        if (btnRedactVisit.id === el.dataset['cardId']) {
            let targetBtn = el;
            let wrapper = el.parentElement.childNodes[0];

            let cardId = targetBtn.dataset['cardId'];
            let userId = targetBtn.dataset['userId'];
            let doc = targetBtn.dataset['doc'];

            let obj = getFormData();
            obj.userId = userId;
            obj.type = 'visit';

            let doctorName = document.querySelector('.doctor-name');
            obj.doctor = doctorName.innerText;
            let prioritiName = document.querySelector('.prioriti-name');
            obj.urgency = prioritiName.innerText;

            for (let name in obj) {
                if (obj[name] === '') {
                    delete obj[name];
                }
            }


            async function putRequest() {
                let putVisit = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(obj)
                })

                let dataVisit = await putVisit.json();

                if (putVisit.ok) {
                    targetBtn.parentElement.children[0].children[1].textContent = obj.name;
                    targetBtn.parentElement.children[0].children[2].textContent = obj.purpose;
                    targetBtn.parentElement.children[0].children[3].textContent = obj.description;

                    if (dataVisit.doctor === 'Кардіолог') {
                        targetBtn.parentElement.children[0].children[0].innerText = 'Кардіолог';
                        targetBtn.parentElement.children[0].children[5].textContent = dataVisit.pressure;

                        let subTitleMassIndex = document.createElement("h5");
                        subTitleMassIndex.innerText = dataVisit.massIndex;
                        subTitleMassIndex.classList.toggle('none')
                        let subTitleHeartWeekness = document.createElement("h5");
                        subTitleHeartWeekness.innerText = dataVisit.heartWeekness;
                        subTitleHeartWeekness.classList.toggle('none')
                        let subTitleAge = document.createElement("h5");
                        subTitleAge.innerText = dataVisit.age;
                        subTitleAge.classList.toggle('none')
                        wrapper.append(subTitleMassIndex, subTitleHeartWeekness, subTitleAge);
                        targetBtn.dataset.doc = 'Кардіолог';

                    } else if (dataVisit.doctor === 'Дантист') {
                        if (doc === 'Кардіолог') {
                            targetBtn.parentElement.children[0].children[8].remove();
                            targetBtn.parentElement.children[0].children[7].remove();
                            targetBtn.parentElement.children[0].children[6].remove();
                            targetBtn.dataset.doc = 'Дантист';
                        }
                        targetBtn.parentElement.children[0].children[0].innerText = 'Дантист';
                        targetBtn.parentElement.children[0].children[5].textContent = dataVisit.visitDate;
                    } else {
                        if (doc === 'Кардіолог') {
                            targetBtn.parentElement.children[0].children[8].remove();
                            targetBtn.parentElement.children[0].children[7].remove();
                            targetBtn.parentElement.children[0].children[6].remove();
                            targetBtn.dataset.doc = 'Терапевт';
                        }

                        targetBtn.parentElement.children[0].children[0].innerText = 'Терапевт'
                        targetBtn.parentElement.children[0].children[5].textContent = dataVisit.age;
                    }

                    if (prioritiName.textContent === 'Невідкладна') {
                        targetBtn.parentElement.children[0].children[4].textContent = 'Невідкладна';
                    } else if (prioritiName.textContent === 'Звичайна') {
                        targetBtn.parentElement.children[0].children[4].textContent = 'Звичайна';
                    } else {
                        targetBtn.parentElement.children[0].children[4].textContent = 'Пріорітетна'
                    }
                }
            }

            putRequest();
        }
    })
})


