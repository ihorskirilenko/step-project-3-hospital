'use strict'
import { Visit } from './visit.js';

export class Dentist extends Visit {

	constructor(obj) {

		super(obj)

		this.doctor = obj.doctor,
		this.visitDate = obj.visitDate

	}
}

