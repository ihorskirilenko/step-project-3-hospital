'use strict'

import { token, api } from './main.js';
import { Visit } from './visit.js';
import { switchButtons } from './main.js';
import { Dentist } from './visitDentist.js';
import { Cardiologist } from './visitCardiologist.js';
import { Therapist } from './visitTherapist.js';

export class User {

    constructor(obj) {

        const { email, password, firstname, lastname, emailSign, passwordSign } = obj;

        this.type = 'user',
            this.email = email || emailSign,
            this.password = password || passwordSign,
            this.name = firstname,
            this.lastname = lastname

    }

    async getData() {

        let users = await fetch(api, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })

        if (users.ok) {
            return await users.json()
        } else {
            throw new Error('Server unavailable')
        }

    }

    async isUser() {

        let data = await this.getData()

        return Array.from(new Set(data.map(el => el.email))).includes(this.email)

    }

    async passConfirm() {

        let data = await this.getData()
        let user = null

        data.forEach(el => {
            if (el.email === this.email && el.password === this.password) user = el;
        })

        return user;

    }

    async renderDeskTop() {

        let data = await this.getData()
        let visits = data.map(el => {
            //console.log(window.localStorage.getItem('userId'))
            if (el.type === 'visit' && window.localStorage.getItem('userId') === el.userId) return el
        })

        visits = visits.filter(el => el !== undefined);

        if(visits.length) {
            visits.forEach(el => {
                el.doctor === 'Дантист' ? new Dentist(el).renderVisit(el.id) : false;
                el.doctor === 'Терапевт' ? new Therapist(el).renderVisit(el.id) : false;
                el.doctor === 'Кардіолог' ? new Cardiologist(el).renderVisit(el.id) : false;
            })

        } else {
            document.querySelector('#noElements').classList.remove('hide')
        }

    }


    async registration() {

        if (!(await this.isUser())) {
            let post = await fetch(api, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(this)
            })

            let user = await post.json()
            window.localStorage.setItem('userId', `${user.id}`)

            switchButtons()
            document.querySelector('#noElements').classList.remove('hide')

        } else {
            throw new Error('User with this email is already registered')
        }

    }

    async signIn() {

        if (await this.passConfirm()) {

            let user = await this.passConfirm()
            window.localStorage.setItem('userId', `${user.id}`)

            switchButtons()

            this.renderDeskTop()

        } else {
            throw new Error('Invalid email or password!')
        }

    }


}



