"use strict"
import { token } from './main.js';
import { api } from './main.js';
import { filterCards } from './cardFilter.js';
// import { redactVisitCard } from './main.js';


export class Visit {
	constructor(obj) {
		this.type = 'visit',
			this.name = obj.name,
			this.purpose = obj.purpose,
			this.description = obj.description,
			this.urgency = obj.urgency
		this.userId = window.localStorage.getItem('userId')
	}


	async postVisit() {
		let postVisit = await fetch(api, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify(this)
		})
		let dataVisit = await postVisit.json()
		let idVisit = dataVisit.id;

		if (postVisit.ok) {
			this.renderVisit(idVisit);
		}
		else {
			alert('Візит не збережено, повторіть збереження візиту!')
		}
	}

	async renderVisit(idVisit) {
		let container = document.querySelector(".wrapper-visit");
		container.classList.add('d-flex', `justify-content-start`, `flex-wrap`, `mb-4`)
		let div = document.createElement("div");
		div.classList.add(`card`, `card-visit`, `mt-2`, `ms-2`, `${this.urgency}`)

		div.innerHTML = `<div class="card-body">
									<div class="d-flex justify-content-between card-body-box">
										<div class="card-info">
										</div>
									</div>
							</div>
				`
		container.prepend(div);

		// let btnCgengeCard = document.querySelector('#redact');
		// btnCgengeCard.dataset.cardId = idVisit;
		// btnCgengeCard.dataset.userId = this.userId;

		// ________create elemen card in loop
		let card = document.querySelector(".card-info")
		let wrapper = document.createElement("div");
		wrapper.classList.add('wrapper-div')
		let box = document.querySelector('.card-body-box')

		for (let prop in this) {
			if (prop == "name" || prop == "doctor") {
				let subTitleVisit = document.createElement("h4");
				subTitleVisit.innerText = this[prop];
				wrapper.insertAdjacentElement('afterbegin', subTitleVisit);
			} else if (prop == "type" || prop == "userId") {
				//	console.log("хуй за щеку, а не тайп");
			} else {
				let subTitleVisit = document.createElement("h5");
				subTitleVisit.classList.add('none')
				subTitleVisit.innerText = this[prop];
				wrapper.append(subTitleVisit);
			}
		}
		// -------	create btn Show More  -----
		let btnLisner = document.createElement('a');
		btnLisner.classList = 'btn btn-secondary btn-show-more mt-4';
		btnLisner.innerText = 'Show more';
		card.append(btnLisner);
		// -------	create btn Change  -----
		let btnChange = document.createElement('a');
		btnChange.classList = 'btn btn-info btn-change mt-4 ms-2 btn-change-card';
		btnChange.setAttribute('data-bs-toggle', 'modal');
		btnChange.setAttribute('data-bs-target', '#visitModal');
		btnChange.dataset.cardId = idVisit;
		btnChange.dataset.userId = this.userId;
		btnChange.innerText = 'Change Visit';

		if (this.doctor === 'Кардіолог') {
			btnChange.dataset.doc = 'Кардіолог';
		} else if (this.doctor === 'Дантист') {
			btnChange.dataset.doc = 'Дантист';
		} else {
			btnChange.dataset.doc = 'Терапевт';
		}
		card.append(btnChange);
		// -------- end -----------
		card.prepend(wrapper);
		// ---- create btn Delete Visit -----
		let btnDeleteLisnear = document.createElement('button');
		btnDeleteLisnear.classList = "btn-close d-flex justify-content-end btn-delete-visit";
		btnDeleteLisnear.setAttribute('type', 'button');
		btnDeleteLisnear.setAttribute('aria-label', 'Закрыть');
		box.append(btnDeleteLisnear);
		// ------ end -----

		this.showMore(btnLisner);
		this.deleteVisit(btnDeleteLisnear, idVisit, div);
		this.changeVisit(btnChange);
	}

	showMore(btnLisner) {
		btnLisner.addEventListener('click', (ev) => {
			let infoCard = ev.target.parentElement.childNodes[0].children;
			let arrayInfoCard = Array.from(infoCard)
			arrayInfoCard.forEach(el => {
				if (el.tagName == 'H5') {
					el.classList.toggle('none')
				}
			})
		})
	}

	deleteVisit(btnDeleteLisnear, id, div) {
		btnDeleteLisnear.addEventListener('click', async () => {
			let deleteRequest = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
				method: 'DELETE',
				headers: {
					'Authorization': `Bearer ${token}`
				},
			})

			await div.remove();
		})
	}

	changeVisit(btnChange) {
		btnChange.addEventListener('click', async function gg (ev) {
// ---display none sub-title card----
			let allBtnChange = document.querySelectorAll('.btn-change-card');
			allBtnChange.forEach(el => {
				let infoCard = el.parentElement.childNodes[0].children;
				let arrayInfoCard = Array.from(infoCard)
				arrayInfoCard.forEach(el => {
					if (el.tagName == 'H5') {
						el.classList.add('none')
					}
				})
			})
// ----end-----
			let btnCreateVisit = document.querySelector('#visit');
			btnCreateVisit.classList.add('none');
			let btnRedactVisit = document.querySelector('.redact');
			btnRedactVisit.classList.remove('none');

			let obj1 = {};
			const container = document.querySelector('.more-information');
			let doctorFild = document.querySelector('.doctor-name');
			const dropdown = document.querySelectorAll('.test');
			let filds = document.createElement('div');

			// dropdown doctor
			let valueDropdawnDoctor = btnChange.parentElement.children[0].children[0].textContent;

			if (valueDropdawnDoctor === 'Кардіолог') {
				doctorFild.innerText = valueDropdawnDoctor;
				obj1.doctor = valueDropdawnDoctor;
				filds.innerHTML = `
					<div class="input-group input-group-sm mb-3 mt-2">
						 <span class="input-group-text" id="inputGroup-sizing-sm">Звичайний тиск</span>
						 <input type="text" class="form-control modal-pressure" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
					</div>
					<div class="input-group input-group-sm mb-3 mt-2">
						 <span class="input-group-text" id="inputGroup-sizing-sm">Індекс маси тіла</span>
						 <input type="text" class="form-control modal-massIndex" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
					</div>
					<div class="input-group input-group-sm mb-3 mt-2">
						 <span class="input-group-text" id="inputGroup-sizing-sm">Хвороби серцево судинної системи</span>
						 <input type="text" class="form-control modal-heart" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
					</div>
					<div class="input-group input-group-sm mb-3 mt-2">
						 <span class="input-group-text" id="inputGroup-sizing-sm">Вік</span>
					<input type="text" class="form-control modal-age" name ="age" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
			  </div>
			  `
				container.innerHTML = '';
				container.append(filds);
				let modalPressure = document.querySelector('.modal-pressure');
				modalPressure.value = btnChange.parentElement.children[0].children[5].textContent;
				obj1.pressure = btnChange.parentElement.children[0].children[5].textContent;

				let modalMassIndex = document.querySelector('.modal-massIndex');
				modalMassIndex.value = btnChange.parentElement.children[0].children[6].textContent;
				obj1.massIndex = btnChange.parentElement.children[0].children[6].textContent;

				let modalHeart = document.querySelector('.modal-heart');
				modalHeart.value = btnChange.parentElement.children[0].children[7].textContent;
				obj1.heartWeekness = btnChange.parentElement.children[0].children[7].textContent;

				let modalAge = document.querySelector('.modal-age');
				modalAge.value = btnChange.parentElement.children[0].children[8].textContent;
				obj1.age = btnChange.parentElement.children[0].children[8].textContent;

			} else if (valueDropdawnDoctor === 'Дантист') {
				doctorFild.innerText = valueDropdawnDoctor;
				obj1.doctor = valueDropdawnDoctor;
				filds.innerHTML = `
                    <div class="input-group input-group-sm mb-3 mt-3">
                        <span class="input-group-text last-visit"  id="inputGroup-sizing-sm">Дата останнього візиту</span>
                        <input type="text" class="form-control modal-visitDate"  name="visitDate" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                `
				container.innerHTML = '';
				container.append(filds);

				let modalVisitDate = document.querySelector('.modal-visitDate');
				modalVisitDate.value = btnChange.parentElement.children[0].children[5].textContent;
				obj1.visitDate = btnChange.parentElement.children[0].children[5].textContent;
			} else {
				doctorFild.innerText = valueDropdawnDoctor;
				obj1.doctor = valueDropdawnDoctor;
				filds.innerHTML = `
				<div class="input-group input-group-sm mb-3 mt-3">
					 <span class="input-group-text" id="inputGroup-sizing-sm">Вік</span>
					 <input type="text" class="form-control modal-age" name = "age" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
				</div>
		  `
				container.innerHTML = '';
				container.append(filds);

				let modalAge = document.querySelector('.modal-age');
				modalAge.value = btnChange.parentElement.children[0].children[5].textContent;
				obj1.age = btnChange.parentElement.children[0].children[5].textContent;
			}

			// inputs
			let modalName = document.querySelector('.modal-name');
			modalName.value = btnChange.parentElement.children[0].children[1].textContent;
			obj1.name = btnChange.parentElement.children[0].children[1].textContent;
			// ---
			let modalPurpose = document.querySelector('.modal-purpose');
			modalPurpose.value = btnChange.parentElement.children[0].children[2].textContent;
			obj1.purpose = btnChange.parentElement.children[0].children[2].textContent;
			// ----
			let modalDescription = document.querySelector('.modal-description');
			modalDescription.value = btnChange.parentElement.children[0].children[3].textContent;
			obj1.description = btnChange.parentElement.children[0].children[3].textContent;
			obj1.type = 'visit';


			// dropdown преоритетність
			let prioritiName = document.querySelector('.prioriti-name');
			let priority = btnChange.parentElement.children[0].children[4].textContent;

			if (priority == 'Невідкладна') {
				obj1.urgency = priority;
				prioritiName.innerText = priority;
			} else if (priority == 'Звичайна') {
				prioritiName.innerText = priority;
				obj1.urgency = priority;
			} else {
				prioritiName.innerText = priority;
				obj1.urgency = priority;
			}
			btnRedactVisit.id = ev.target.dataset['cardId'];
		})
	}
}

