'use strict'

import { Visit } from './visit.js';

export class Cardiologist extends Visit {

	constructor(obj) {

		super(obj)

		this.doctor = obj.doctor,
		this.pressure = obj.pressure
		this.massIndex = obj.massIndex
		this.heartWeekness = obj.heartWeekness
		this.age = obj.age

	}
}

