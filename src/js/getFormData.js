
let testObj = {};
const dropdown2 = document.querySelectorAll('.test');
const priority = document.querySelectorAll('.prioriti');

dropdown2.forEach(element => {
    element.addEventListener('click', () => {
        if (element.dataset.value == 'Кардіолог') {
            testObj.doctor = element.dataset.value
        } else if (element.dataset.value == 'Дантист') {
            testObj.doctor = element.dataset.value;
        } else {
            testObj.doctor = element.dataset.value
        }
    })
})
priority.forEach(element => {
    element.addEventListener('click', () => {
        if (element.dataset.value == 'Невідкладна') {
            testObj.urgency = element.dataset.value;
        } else if (element.dataset.value == 'Звичайна') {
            testObj.urgency = element.dataset.value;
        } else {
            testObj.urgency = element.dataset.value;
        }
    })
})


export function getFormData() {

    let input = document.querySelectorAll('input');
    for (let i = 0; i < input.length; i++) {
        const { name, value } = input[i];
        testObj[name] = value;
    }
    return testObj;
}

export function resetVisitModal(form, close) {
    let modalContent = document.querySelectorAll('.modal');

    modalContent.forEach(element => {
        element.addEventListener('click', (event) => {
            if (event.target.className == 'modal fade show') {
                form.reset()
            }
        })
    })
    close.addEventListener('click', () => {
        form.reset()
    })
}