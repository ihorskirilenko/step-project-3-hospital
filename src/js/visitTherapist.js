'use strict'
import { Visit } from './visit.js';

export class Therapist extends Visit {

    constructor(obj) {

        super(obj)

        this.doctor = obj.doctor,
        this.age = obj.age

    }
}

