export const modalVisit = () => {

    function modalCreateVisit() {

        const container = document.querySelector('.more-information');
        let doctorFild = document.querySelector('.doctor-name');
        const priority = document.querySelectorAll('.prioriti');
        const dropdown = document.querySelectorAll('.test');
        let prioritiName = document.querySelector('.prioriti-name');



        priority.forEach(element => {
            element.addEventListener('click', () => {
                if (element.dataset.value == 'Невідкладна') {
                    prioritiName.innerText = element.dataset.value
                } else if (element.dataset.value == 'Пріорітетна') {
                    prioritiName.innerText = element.dataset.value
                } else {
                    prioritiName.innerText = element.dataset.value
                }
            })
        })

        dropdown.forEach(element => {
            let filds = document.createElement('div');
            element.addEventListener('click', () => {
                container.innerHTML = ''
                if (element.dataset.value == 'Кардіолог') {
                    doctorFild.innerText = element.dataset.value;
                    filds.innerHTML = `
                    <div class="input-group input-group-sm mb-3 mt-2">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Звичайний тиск</span>
                        <input type="text" class="form-control modal-pressure" name="pressure" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="input-group input-group-sm mb-3 mt-2">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Індекс маси тіла</span>
                        <input type="text" class="form-control modal-massIndex" name="massIndex" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="input-group input-group-sm mb-3 mt-2">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Хвороби серцево судинної системи</span>
                        <input type="text" class="form-control modal-heart" name="heartWeekness" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="input-group input-group-sm mb-3 mt-2">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Вік</span>
                    <input type="text" class="form-control modal-age" name ="age" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
                `
                    container.append(filds)

                } else if (element.dataset.value == 'Дантист') {
                    doctorFild.innerText = element.dataset.value;
                    filds.innerHTML = `
                    <div class="input-group input-group-sm mb-3 mt-3">
                        <span class="input-group-text last-visit"  id="inputGroup-sizing-sm">Дата останнього візиту</span>
                        <input type="text" class="form-control modal-visitDate"  name="visitDate" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                `
                    container.append(filds);
                } else {
                    doctorFild.innerText = element.dataset.value;
                    filds.innerHTML = `
                    <div class="input-group input-group-sm mb-3 mt-3">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Вік</span>
                        <input type="text" class="form-control modal-age" name = "age" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    </div>
                `
                    container.append(filds);
                }
            })
        })
    }
    modalCreateVisit()
}
