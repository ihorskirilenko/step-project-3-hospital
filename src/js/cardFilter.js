export const filterCards = async () => {
    setTimeout(() => {
        let cards = document.querySelectorAll('.card-visit');
        let prioriti = document.querySelectorAll('.filter-item');
        prioriti.forEach(el => {
            el.addEventListener('click', (event) => {
                let atr = event.target.dataset['f']
                cards.forEach(card => {
                    card.classList.remove('hide');

                    if (!card.classList.contains(atr)) {
                        card.classList.add('hide')
                    }
                })
            })

        })
    }, 1000)


    setTimeout(() => {
        const inputField = document.querySelector('.search');
        inputField.addEventListener('input', () => {
            let val = inputField.value.trim().toLowerCase();
            let cards = document.querySelectorAll('.card-visit h5:nth-child(4)');
            if (val != '') {
                cards.forEach(element => {
                    element.parentNode.parentNode.parentNode.parentNode.parentNode.classList.remove('hide')
                    if (element.innerText.search(val) == -1) {
                        element.parentNode.parentNode.parentNode.parentNode.parentNode.classList.add('hide')
                    } else {
                        element.parentNode.parentNode.parentNode.parentNode.parentNode.classList.remove('hide');
                    }
                })
            } else {
                cards.forEach(element => {
                    element.parentNode.parentNode.parentNode.parentNode.parentNode.classList.remove('hide')
                })
            }
        }, 1000)
    })
}