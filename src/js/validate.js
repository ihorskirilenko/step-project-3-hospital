
export function validate() {
    let inputs = registration.getElementsByTagName('input');
    let pas1 = document.querySelector('.password')
    let pas2 = document.querySelector('.confirmPassword')
    for (let i = 0; i < inputs.length; i++) {

        inputs[i].classList.remove('is-invalid')

        if (pas1.value == pas2.value) {
            pas1.classList.remove('is-invalid')
            pas2.classList.remove('is-invalid')
            return true
        } else {
            pas1.classList.add('is-invalid')
            pas2.classList.add('is-invalid')
            return false
        }
    }
}




