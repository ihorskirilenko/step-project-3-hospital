'use strict'

import { api, token } from "./main.js";

export async function deleteAllData() {

    let users = await fetch(api, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        },

    })

    let data = await users.json()

    for (let obj of data) {

        let deletion = await fetch(`${api}/${obj.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            },

        })

    }

}
